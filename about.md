---
title: "关于我"
layout: about
image:  assets/images/banner_about.jpg
---

### 基本信息
##### 姓名：涂璐璐
##### 身份：大一网络与新媒体人
##### 年龄：19
##### 就读于中山大学南方学院 文学与传媒学院 学生
***
### 学习课程：
* 《网页设计与制作》
* 《数字媒体技术与协作》  
 
 ***
### 其他作品：
   [个人简历](https://tululunewmedia.gitee.io/resume/)