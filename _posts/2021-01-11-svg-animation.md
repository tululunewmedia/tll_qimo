---
title: svg让你快速获取点赞-小白速成
layout: page
date: 2021-01-11
excerpt_separator: "<!--more-->"
categories:
     - SVG制作
tags:
  - SVG
---
标签: SVG使用 关键帧的设置  伸缩  Markdown
<!--more-->

## 静态点赞图标
是我在[ **icon font** ](https://www.iconfont.cn/)上找到的一个图标，就是下面的这个东西。
![点赞图标](https://riyugo.com/i/2021/01/11/aehulv.png)
<style>
	.tags svg{
             animation: tags 5s infinite alternate ;
             position:relative;
			 width: 100px;
		     height: 100px;
		     background: red;
		     position: relative;
		     animation: tags 5s;
		-webkit-animation: tags 5s;
		/* Safari and Chrome */
	}

	@keyframes tags {
		0% {
			transform: scale(0.5);
			background: red;
			left: 0px;
			top: 0px;
		}

		25% {
			transform: scale(1.2);
			background: yellow;
			left: 200px;
			top: 0px;
		}

		50% {
			transform: scale(1.0);
			background: blue;
			left: 200px;
			top: 50px;
		}

		75% {
			transform: scale(0.5);
			background: green;
			left: 250px;
			top: 0px;
		}

		100% {
			transform: scale(1.0);
			background: red;
			left: 0px;
			top: 0px;
		}
	}

	@-webkit-keyframes tags

	/* Safari and Chrome */
		{
		0% {
			background: red;
			left: 0px;
			top: 0px;
		}

		25% {
			background: yellow;
			left: 200px;
			top: 0px;
		}

		50% {
			background: blue;
			left: 200px;
			top: 50px;
		}

		75% {
			background: green;
			left: 250px;
			top: 0px;
		}

		100% {
			background: red;
			left: 0px;
			top: 0px;
		}
	}
</style>
<div class="tags">
<svg width="50%" aria-hidden="true" focusable="false" data-prefix="far" data-icon="tags" class="icon"
	 role="img" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 1024 1024"><path d="M758.613333 832H325.973333a21.333333 21.333333 0 0 1-21.333333-21.333333V355.84a21.333333 21.333333 0 0 1 21.333333-21.333333h8.96l99.413334-158.933334A85.333333 85.333333 0 0 1 595.84 213.333333v121.6h166.826667a100.906667 100.906667 0 0 1 75.52 34.346667 104.96 104.96 0 0 1 25.386666 82.986667l-43.946666 325.973333A62.08 62.08 0 0 1 758.613333 832z m-411.306666-42.666667h411.306666a19.413333 19.413333 0 0 0 18.773334-17.28L821.333333 448a62.293333 62.293333 0 0 0-15.146666-49.28 57.386667 57.386667 0 0 0-42.666667-19.84h-188.586667a21.333333 21.333333 0 0 1-21.333333-21.333333V213.333333a42.666667 42.666667 0 0 0-80.64-17.493333l-1.28 2.346667-106.666667 170.666666a21.333333 21.333333 0 0 1-17.493333 10.026667z" fill="#333333" p-id="813">
</path></svg>
</div>

## 使用要点
- 使用keyframes实现颜色的变化
- 使用transform: scale实现图标大小的改变
