---
title: 几行代码如何让你的svg图形更加突出
layout: page
date: 2021-01-11
excerpt_separator: "<!--more-->"
categories:
    - SVG制作
tags:
    - SVG
---
标签: SVG制作 方形 伸缩  旋转  Markdown
<!--more-->

## 下图为：方形变大、随着时间开始旋转

*动画效果：伸缩、旋转*


<svg xmlns="http://www.w3.org/2000/svg">
  <rect id="square"
        x="50" y="20"
        width="50" height="50" rx="5"
        fill="#5F9EA0">
  </rect>
  <animateTransform
                    xlink:href="#square"
                    attributeName="transform"
                    begin="0s"
                    dur="3s"
                    type="scale"
                    from="1" to="2"
                    repeatCount="indefinite" />
  <animateTransform
                       attributeName="transform"
                       attributeType="XML"
                       type="rotate"
                       from="0 0 0"
                       to="360 0 0"
                       dur="2"
                       repeatCount="indefinite" />
</svg>

以下为代码具体含义：
```
<svg xmlns="http://www.w3.org/2000/svg">
  <rect id="square"
        x="50" y="20"
        width="50" height="50" rx="5"    ！设置方形的x，y值，宽，高和圆角
        fill="#5F9EA0">
  </rect>
  <animateTransform
                    xlink:href="#square"     
                    attributeName="transform"
                    begin="0s"
                    dur="3s"
                    type="scale"
                    from="1" to="2"        ！设置方形的缩放时间和缩放比例
                    repeatCount="indefinite" />
  <animateTransform
                       attributeName="transform"
                       attributeType="XML"
                       type="rotate"
                       from="0 0 0"
                       to="360 0 0"        ！设置方形的旋转角度和持续时间
                       dur="3"               
                       repeatCount="indefinite" />
</svg>
```

*由于个人对svg不够了解，如有疑问，欢迎补充。*